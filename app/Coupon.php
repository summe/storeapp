<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
class Coupon extends Model
{
  use Sluggable;
 use SluggableScopeHelpers;
  public function page()
  {
    return $this->belongsTo('App\Page');
  }
  public function comments()
  {
    return $this->hasMany('App\Comment');
  }
  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'title'
          ]
      ];
  }
  public function scopeOnSlider($query)
  {
      return $query->where('add_to_home_slider', true);
  }
  public function scopeOnHomeBox($query)
  {
      return $query->where('add_to_home_box', true);
  }
}
