<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use App\SocialFacebookAccount;
use App\User;
use Validator;

class SocialAuthFacebookController extends Controller
{
    public function __construct(){
      $this->middleware('guest', ['only' => ['callback']]);

    }
    public function redirect ()
    {
      return Socialite::driver('facebook')->redirect();
    }


    public function callback(SocialFacebookAccountService $service)
    {
        $providerUser = Socialite::driver('facebook')->user();
        $user = $service->findUser($providerUser);
        if($user){
          auth()->login($user);
          return redirect()->back();
        } else {
          return view('auth.social')
            ->with('user', $providerUser);
        }

    }

    public function createSocialUser(){
      return view('auth.social');
    }

    public function saveSocialUser(Request $request)
    {
      $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255',
        'last_name' => 'required|string',
        'phone' => 'required|string',
        'gender' => 'required',
        'birthday' => 'required',
        'full_address' => 'required',
        'provider_user_id' => 'required'
      ]);

      if ($validator->fails()) {
       return view('auth.social');

      }
      $data = $request->all();
      $account = new SocialFacebookAccount([
          'provider_user_id' => $data['provider_user_id'],
          'provider' => 'facebook'
      ]);

      $ip_data = geoip()->getLocation(\Request::ip());
      $neighborhood = "{$ip_data->country}, {$ip_data->state}, {$ip_data->city}," .
      " {$ip_data->iso_code}, {$ip_data->postal_code}";
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt(md5(rand(9999,100000))),
            'last_name' => $data['last_name'],
            'phone' => $data['phone'],
            'gender' => $data['gender'],
            'birthday' => $data['birthday'],
            'full_address' => $data['full_address'],
            'neighborhood' => $neighborhood,
            'coupons_limit' => 100,
            'coupons_used' => 0,
        ]);
        $account->user()->associate($user);
        $account->save();
        auth()->login($user);
        return redirect('/');
    }
}
