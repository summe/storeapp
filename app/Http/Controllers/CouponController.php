<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($pageid)
    {
      $page = Page::findBySlug($pageid);
      $coupons = $page->coupons()->get();
      return view('coupons.show', ['page' => $page, 'coupons' => $coupons]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($pageid, $couponid)
    {
      $page = Page::findBySlug($pageid);
      $coupon = $page->coupons()->where('slug', $couponid)
        ->first();

        return view('coupons.show')
          ->with('page', $page)
          ->with('coupon', $coupon);
    }

    public function coupon_used()
    {
      $user = \App\User::find(auth()->user()->id);
      $user->coupons_used = $user->coupons_used + 1;
      $user->save();
      return response()
            ->json(['coupon_used' => $user->coupons_used]);
    }

}
