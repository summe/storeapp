<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index()
    {
      $summary = 0;
      foreach ((array)session('cart') as $k => $v) {
        $summary += \App\Coupon::find($k)->price * $v;
      }
      return view('cart.index')
        ->with('summary', $summary);
    }

    public function addToCart(Request $request)
    {
        $cart = (array)session('cart');
        $id = $request->get('product_id');
        $count = $request->get('count');
        if (key_exists($id, $cart)){
          $count = $cart[$id] + $count;
        }
        $cart[$id] = $count;
        session(['cart' => $cart]);
        return response()
          ->json(['status'=>'success']);
    }

    public function removeFromCart(Request $request)
    {
        $cart = (array)session('cart');
        $id = $request->get('product_id');
        if (key_exists($id, $cart)){
          unset($cart[$id]);
        }
        session(['cart' => $cart]);
        return response()
          ->json(['status'=>'success']);
    }

    public function getCart()
    {
        $cart = (array)session('cart');
    }

}
