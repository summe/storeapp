<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\GeneralPage;
use Illuminate\Support\Facades\Auth;

class GeneralPagesController extends Controller
{
  public function show($id)
  {
    $info = \App\GeneralPage::findBySlug($id);
      return view('general.show', ['info'=> $info]);
  }
  public function authenticate(Request $request)
  {
      $email = $request->get('email');
      $password = $request->get('password');
      if (Auth::attempt(['email' => $email, 'password' => $password])) {
        return redirect()->back();
      } else {
        return redirect()->to('/login');
      }
  }
}
