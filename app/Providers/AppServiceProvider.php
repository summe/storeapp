<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Page;
use App\GeneralPage;
use App\SiteSetting;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      view()->share('pages', Page::all());
      view()->share('nav', GeneralPage::all());
      view()->share('_settings', SiteSetting::first());
    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
