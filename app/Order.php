<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable= [
      'name',
      'last_name',
      'email',
      'phone',
      'coupon_id',
      'count',
    ];
}
