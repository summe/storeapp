<?php

namespace App\Services;
use App\SocialFacebookAccount;
use App\User;
use Laravel\Socialite\Contracts\User as ProviderUser;

class SocialFacebookAccountService
{
    public function findUser(ProviderUser $providerUser)
    {
      $account = SocialFacebookAccount::whereProvider('facebook')
          ->whereProviderUserId($providerUser->getId())
          ->first();
      if ($account) {
          return $account->user;
      }
      return false;
    }

    public function createUser(ProviderUser $providerUser)
    {
        $account = new SocialFacebookAccount([
            'provider_user_id' => $providerUser->getId(),
            'provider' => 'facebook'
        ]);

        $user = User::whereEmail($providerUser->getEmail())->first();

        if (!$user) {
            $email =  $providerUser->getEmail();
            $email = $email ?: "user#{$providerUser->getId()}@ex.com";
            $user = User::create([
                 'email' => $email ,
                 'name' => $providerUser->getName(),
                 'password' => md5(rand(1,10000)),
             ]);
        }

         $account->user()->associate($user);
         $account->save();
         return $user;
    }
}
