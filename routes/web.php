<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('comments', 'CommentController');
Route::get('/', 'PageController@index');

Route::get('/info/{id}', 'GeneralPagesController@show')->name('general');
Route::post('/authenticate', 'GeneralPagesController@authenticate')->name('manual_auth');

Auth::routes();

Route::get('/redirect', 'SocialAuthFacebookController@redirect')
  ->name('fb_login');
Route::get('/callback', 'SocialAuthFacebookController@callback');

Route::get('/home', 'HomeController@index')->name('home');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::resource('pages', 'PageController');
Route::resource('orders', 'OrderController');
Route::resource('pages.coupons', 'CouponController',
  ['only' => ['index', 'show']]
);
Route::post('/add-to-cart', 'CartController@addToCart')
  ->name('add-to-cart');
Route::post('/remove-from-cart', 'CartController@removeFromCart')
  ->name('remove-from-cart');
  Route::get('/cart', 'CartController@index')
    ->name('cart');
Route::post('/coupon_used', 'CouponController@coupon_used')->name('use_coupon');
Route::post('/social_store', 'SocialAuthFacebookController@saveSocialUser')->name('social_store');
Route::get('/social_create', 'SocialAuthFacebookController@createSocialUser')->name('create-social-user');
