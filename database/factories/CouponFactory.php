<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Coupon::class, function (Faker $faker) {
    return [
        'title' => $faker->word,
        'description' => $faker->sentence,
        'price' => $faker->numberBetween(1000, 5000),
        'details' => $faker->text,
        'qr_link' => $faker->url,
        'image' =>$faker->imageUrl(1920, 600),
        'page_id' => App\Page::all()->random()->id,
        'latitude' => $faker->latitude,
        'longitude' => $faker->longitude,
    ];
});
