<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      factory(App\Page::class, 8)->create()->each(function ($p) {
        factory(App\Coupon::class, 10)->create()->each(function ($c) use($p) {
          $p->coupons()->save($c);
        });
      });
    }
}
