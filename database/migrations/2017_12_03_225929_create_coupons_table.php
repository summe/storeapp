<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('title');
            $table->text('description');
            $table->string('image');
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('pages');
            $table->text('details');
            $table->float('price');
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('qr_link')->nullable();
            $table->timestamps();
        });
    }
    // id	integer	NO	PRI		auto_increment
    // title	varchar	NO
    // description	text	NO
    // image	varchar	NO
    // expired_at	date	NO
    // page_id	integer	NO
    // details	text	NO
    // latitude	varchar	YES
    // longitude	varchar	YES
    // qr_link	varchar	YES
    // created_at	timestamp	YES
    // updated_at	timestamp	YES
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
