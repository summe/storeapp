<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyToPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('coupons', function (Blueprint $table) {
        $table->boolean('add_to_home_slider')->default(false);
        $table->boolean('add_to_home_box')->default(false);
      });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('coupons', function (Blueprint $table) {
        $table->dropColumn('add_to_home_slider');
        $table->dropColumn('add_to_home_box');
      });
    }
}
