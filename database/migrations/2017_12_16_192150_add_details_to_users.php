<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDetailsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function(Blueprint $table){
          $table->string('last_name')->nullable();
          $table->string('phone')->nullable();
          $table->string('gender')->nullable();
          $table->string('birthday')->nullable();
          $table->string('full_address')->nullable();
          $table->string('neighborhood')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('users', function(Blueprint $table){
        $table->dropColumn('last_name');
        $table->dropColumn('phone');
        $table->dropColumn('gender');
        $table->dropColumn('birthday');
        $table->dropColumn('full_address');
        $table->dropColumn('neighborhood');
      });
    }
}
