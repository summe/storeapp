<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFaFaIconClassToGeneralPages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('general_pages', function (Blueprint $table) {
        $table->string('fa_fa_icon');
        $table->string('preview_text');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('general_pages', function (Blueprint $table) {
        $table->dropColumn('preview_text');
        $table->dropColumn('fa_fa_icon');
      });
    }
}
