@extends('layouts.app')
@section('content')
  <div class="main-content">
    <div class="first-content-block">
      <div class="main-container">
          <div  id="carousel">
            @foreach ($s_coupons as $coupon)
              <div class="description-content">
                <img src="{{ $coupon->image }}" alt="" >
                <div class="slide-description">
                  <a href="{{ route('pages.coupons.show',
                          ['pageid'=> $coupon->page()->first()->slug,
                            'couponid'=> $coupon->slug])}}">
                    <h2>{{ $coupon->title }}</h2>
                  </a>
                  {{-- <a href="{{ route('pages.coupons.show')}}">

                  </a> --}}
                </div>
              </div>
            @endforeach
        </div>
      </div>
    </div>
    <div class="second-content-block">
      <div class="main-container">
        @include('shared.nav-box')
        <div class="main-coupons">
            @foreach ($box_coupons as $coupon)
              <div class="page-item first">
                @if($loop->first)
                  <div class="item-content first">
                @else
                  <div class="item-content">
                @endif
                  <div class="item-description">
                    <h3>{{ $coupon->title }}</h3>
                    <p> {!! $coupon->details !!}</p>
                    <a href="{{ route('pages.coupons.show',
                            ['pageid'=> $coupon->page()->first()->slug,
                              'couponid'=> $coupon->slug])}}">Link</a>
                  </div>
                  <div class="item-logo-box">
                    {{-- <img src="{{ Storage::url($coupon->image)}}" alt="" class="item-logo"> --}}
                    <img src="{{$coupon->image}}" alt="" class="item-logo">
                  </div>
                </div>
              </div>
          @endforeach
        </div>
      </div>
    </div>
    <div class="third-content-block">
      @foreach ($pages as $page)
        @if($page->coupons()->count() > 0)
          <div class="page-items-container">
            <div class="content-slider">
              <div class="title-box">
                <div class="title-item">
                  {{ $page->topic}}
                </div>
                <div class="heading-subtitle">
                  <p>{{ $page->note}}</p>
                </div>
              </div>
              <div class="category"  id="ltwo{{$page->id}}">
                @foreach ($page->coupons()->get() as $coupon)
                  @include('shared.coupon-preview',
                    ['coupon' => $coupon, 'page' => $page ]
                  )
                @endforeach
              </div>
              @if($page->coupons()->count() > 1)
                <script type="text/javascript">
                  $(document).ready(function(){
                    $('#ltwo{{$page->id}}').slick({
                       dots: true,
                       slidesToShow: 3,
                       slidesToScroll: 1,
                       autoplay: true,
                       responsive: [{
                          breakpoint: 1024,
                          settings: {
                            slidesToShow: 2,
                            arrows: false,
                            variableWidth: false
                          }
                        }, {

                          breakpoint: 768,
                          settings: {
                            slidesToShow: 1,
                          }

                        }, {

                          breakpoint: 300,
                          settings: "unslick"
                        }]
                    });
                  })
                </script>
              @endif
            </div>
          </div>
        @endif
      @endforeach
    </div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#carousel').slick({
         dots: true,
         arrows: true,
         fade: true,
         speed: 500,
         autoplay: true
      });
    })
  </script>

@endsection
