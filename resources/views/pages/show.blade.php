@extends('layouts.app')
@section('content')
  <div class="main-content">
    <div class="content-box">
      <div class="title-box">
        <div class="title-item">
          {{ $page->topic}}
        </div>
        <div class="heading-subtitle">
          <p>{{ $page->note}}</p>
        </div>
      </div>
      <div class="category category-show"  id="ltwo{{$page->id}}">
        @foreach ($page->coupons()->get() as $coupon)
          @include('shared.coupon-preview',
            ['coupon' => $coupon, 'page' => $page ]
            )
        @endforeach
      </div>
    </div>
  </div>
@endsection
