@extends('layouts.app')
@section('content')
  <h1 class='header-title-main'>Cart</h1>
  <table class='table table-striped'>
    <thead>
      <tr>
        <th>Product</th>
        <th>Count</th>
        <th>Summary</th>
        <th>Remove</th>
      </tr>
    </thead>
    <tbody>
      @foreach ((array)session('cart') as $key => $val)
        <tr>
          <td>{{ \App\Coupon::find($key)->title }}</td>
          <td>{{ $val}}</td>
          <td>{{ \App\Coupon::find($key)->price * $val .'$'}}</td>
          <td>
            <button
              class="btn btn-danger"
              onclick="handleRemove(this,{{ \App\Coupon::find($key)->id }})"
            >X</button>
          </td>
        </tr>
      @endforeach
      @if(count((array)session('cart'))>0)
        <tr>
          <td> Summary </td>
          <td>{{ $summary.'$' }}</td>
          <td>
            <a href="{{ route('orders.create')}}" class='btn btn-success'> Process</a>
          </td>
        </tr>
      @endif
    </tbody>

  </table>
  <script type="text/javascript">
    function handleRemove(event, id){
      console.log('clicked');
        $.ajax({
          method: "POST",
          url: "{{ route('remove-from-cart')}}",
          headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
          data: { product_id: id }
        })
      .done(function( response ) {
        location.reload();
      });
    }
  </script>
@endsection
