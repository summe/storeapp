@extends('layouts.app')
@section('content')
  <div class="main-content">
    <div class="">
      <div class="inner-content head">
        <div class="item-heading">
          {{ $coupon->title }}
        </div>
      </div>
    </div>
    <div class="inner-container image-block">
      <div class="inner-content">
        <div class="item-container">
          <div class="item-content">
            <div class="item-image">
              {{-- <img src="{{ Storage::url($coupon->image)}}" alt="" class=""> --}}
              <img src="{{$coupon->image }}" alt="" class="">
            </div>
          </div>
        </div>
      </div>
      </div>
      <div class="inner-container">
      <div class="inner-content">
        <div class="item-container">
          <div class="item-content">
            <div class="image-description">
              <div class="item-text-box">
                <h2>{{ $coupon->title }}</h2>
                <p>{!!  $coupon->description !!}</p>
                <h4 class='product-price'>{{ 'Price: $' . $coupon->price }}</h4>
                <div class="">
                  <label for="">Persons </label>
                  <div class="">
                    <input type="number" name="count" value="1" min="1" id="count" class="persons-counter"><br>
                  </div>
                </div>
                <p></p>
                <a href="#" class='btn btn-primary' id='add-to-cart'>Add to cart</a>
                <p></p>
                @guest
                  @include('shared.login-popup')
                @endguest
              </div>
            </div>
            {{-- @include('shared.map', ['coupon' => $coupon]) --}}
          </div>
          <div class="comments-block">

            <h2 class='comments-title'>COMMENTS</h2>
            <div class="">
              @foreach ($coupon->comments as $comment)
                <div class="comment-box">
                  <p class="comment-author">{{ $comment->user->email . '&bull;' }}
                  <span class="comment-time">{{ $comment->created_at->diffForHumans() }}</span></p>
                  <p class="comment-text">{{ $comment->text }}</p>
                </div>
              @endforeach
            </div>
            @auth
              @include('shared.comment-form', ['coupon' => $coupon])
            @endauth
          </div>
        </div>
      </div>
    </div>
    <div class="map-box">
      <div class="inner-content">
      </div>
    </div>

  </div>
  <script type="text/javascript">
  document.addEventListener("DOMContentLoaded", function(event) {
    $('#add-to-cart').click(function(){
      $.ajax({
        method: "POST",
        url: "{{ route('add-to-cart')}}",
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: { product_id: "{{ $coupon->id}}", count: $('#count').val() }
      })
    .done(function( response ) {
      location.reload();
    });
  });
  })
  </script>
@endsection
