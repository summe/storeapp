@extends('layouts.app')

@section('content')
<div class="container auth-block">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}
                        <div class="input-group-box">
                          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                  <input id="name" type="text" placeholder="First Name" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                  @if ($errors->has('name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                  <input id="last_name"   type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ old('last_name') }}" required>

                                  @if ($errors->has('last_name'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('last_name') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                        </div>
                        <div class="input-group-box">
                          <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                  <input id="email" placeholder="Email"  type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                  @if ($errors->has('email'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('email') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">

                              <div class="col-md-12">
                                  <input id="phone" placeholder="Phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required>

                                  @if ($errors->has('phone'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('phone') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                        </div>
                        <div class="input-group-box">
                          <div class="form-group{{ $errors->has('gender') ? ' has-error' : '' }}">
                              <label for="gender" class="col-md-4 control-label">Gender</label>

                              <div class="col-md-6 checkbox-items">
                                  <input type="radio" name="gender" value="male" checked> Male
                                  <input type="radio" name="gender" value="female" > Female<br>
                                  @if ($errors->has('gender'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('gender') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group{{ $errors->has('birthday') ? ' has-error' : '' }}">
                              <div class="col-md-12 ">
                                  <input id="birthday" placeholder="Birthday" type="text" class="form-control" name="birthday" value="{{ old('birthday') }}" required>

                                  @if ($errors->has('birthday'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('birthday') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                        </div>
                        <div class="input-group-box">
                          <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                  <input id="password" placeholder="Password" type="password" class="form-control" name="password" required>

                                  @if ($errors->has('password'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('password') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                          <div class="form-group{{ $errors->has('full_address') ? ' has-error' : '' }}">
                              <div class="col-md-12">
                                  <input id="full_address" placeholder="Full address" type="text" class="form-control" name="full_address" value="{{ old('full_address') }}" required>

                                  @if ($errors->has('full_address'))
                                      <span class="help-block">
                                          <strong>{{ $errors->first('full_address') }}</strong>
                                      </span>
                                  @endif
                              </div>
                          </div>
                        </div>
                        <div class="input-group-box">

                          <div class="form-group">
                              <div class="col-md-12">
                                  <input id="password-confirm"  placeholder="Password confirm" type="password" class="form-control" name="password_confirmation" required>
                              </div>
                          </div>
                        </div>
                        @if ($errors->has('neighborhood'))
                            <span class="comment-help-block  help-block">
                                <strong>{{ $errors->first('neighborhood') }}</strong>
                            </span>
                        @endif
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
