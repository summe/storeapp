<li><a href="/">Home</a></li>
@foreach ($nav as $nav_item)
  <li><a href="{{ route('general', ['id'=> $nav_item->slug ])}}">
    {{ $nav_item->link_name }}
  </a></li>
@endforeach
