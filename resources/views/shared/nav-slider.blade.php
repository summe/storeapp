<div class="nav-slider small">
  <div class="nav-slider-text">
    @foreach ($info_pages as $info)
      <a href="{{ route('general', ['id' => $info->slug])}}" class="nav-slider-link">
        <div class="nav-slider-item">
          <div class="slider-icon">
            <i class="fa {{ $info->fa_fa_icon}}" aria-hidden="true"></i>
          </div>
          <h4>
            {{ $info->title }}
          </h4>
          <p>
            {{ $info->preview_text }}
          </p>
        </div>
      </a>
    @endforeach
  </div>
</div>
