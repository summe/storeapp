<div class="coupon-preview">
  <div class="preview-logo">
    <div class="logo-overfloww"></div>
    {{-- <img src="{{ Storage::url($coupon->image)}}" alt="" > --}}
    <img src="{{ $coupon->image }}" alt="" >
  </div>
  <div class="preview-text">
    <a href="{{ route('pages.coupons.show', [$page->slug, $coupon->slug])}}"><h3>{{ $coupon->title}}</h3>
    <p>{{ $coupon->note}}</p>
    </a>
  </div>
</div>
