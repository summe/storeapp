<div class="header" id="header">
  <div class="header-lang">
    <div class="nav-sm-left">
      <div class="nav-links">
        <nav class="header-nav">
          <ul>
            @guest
              <li><a href="{{ route('fb_login')}}" class="facebook-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li><a href="{{ route('login')}}" class="login-link"><i class="fa fa-user" aria-hidden="true"></i></a></li>
              <li><a href="{{ route('register') }}" class="logout-link"><i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
            @else
              <li>
                  <a href="{{ route('logout') }}"
                      onclick="event.preventDefault();
                               document.getElementById('logout-form').submit();">
                      Logout
                  </a>

                  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
              </li>
            @endguest
          </ul>
        </nav>
      </div>
    </div>
  </div>
  <div class="header-content">
    <div class="header-brand">
      Store
    </div>
    <div class="header-menu">
      <a href="#" id="menu-toggle">
        <div class="header-menu-item">
          <i class="fa fa-bars" aria-hidden="true"></i>
          <h3>MENU</h3>
        </div>
      </a>
      <a href="/">
        <div class="header-menu-item">
          <i class="fa fa-home" aria-hidden="true"></i>
          <h3>HOME</h3>
        </div>
      </a>
      <a href="#">
        <div class="header-menu-item">
          <i class="fa fa-question-circle" aria-hidden="true"></i>
          <h3>ABOUT</h3>
        </div>
      </a>
      <a href="{{ route('login')}}">
        <div class="header-menu-item">
          <i class="fa fa-user" aria-hidden="true"></i>
          <h3>LOGIN</h3>
        </div>
      </a>
      <a href="{{ route('cart') }}">
        <div class="header-menu-item">
          <i class="fa fa-shopping-cart" aria-hidden="true"></i>
          <h3>CART {{ count((array)session('cart'))}}</h3>
        </div>
      </a>
    </div>
  </div>
</div>
<div class="">
  <div class="nav-sidebar-container close-slider" id="nav-slider">
    <div class="nav-sidebar">
      <ul>
        <li class="nav-sidebar-item">
          <a href='#' class='sidebar-close-button close-slider' id='' >
            X
          </a>
        </li>
        @foreach ($pages as $page)
          <li class="nav-sidebar-item">
            <a href="{{ route('pages.show',['pageId'=> $page->slug])}}">
              {{ $page->title }}
            </a>
          </li>
        @endforeach
      </ul>
    </div>
  </div>
</div>
<script type="text/javascript">

  window.onscroll = function() {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;
    var header = document.getElementById('header')
    var content = document.getElementById('content')
    var classList = header.classList
    if ( scrolled > 20 && !classList.contains('fixed-header')){
      header.classList.add('fixed-header');
    } else if(scrolled <= 20 && classList.contains('fixed-header')) {
      document.getElementById('header').classList.remove('fixed-header');
    }
  }
</script>
