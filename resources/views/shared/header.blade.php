<div class="header">
  <div class="nav-sm-container">
    <div class="nav-sm">
      <div class="nav-sm-right">
        <div class="nav-links">

          <nav class="header-nav">
            <ul class="drop-menu-block">
              @include('shared.dropdown')
            </ul>
            <ul class="large-menu-block">
              @include('shared.main_nav')
            </ul>
        </nav>

        </div>
      </div>
      <div class="nav-sm-left">
        <div class="nav-links">
          <nav class="header-nav">
            <ul>
              @guest
                <li><a href="{{ route('fb_login')}}" class="facebook-link"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                <li><a href="{{ route('login')}}" class="login-link"><i class="fa fa-user" aria-hidden="true"></i></a></li>
                <li><a href="{{ route('register') }}" class="logout-link"><i class="fa fa-user-plus" aria-hidden="true"></i></a></li>
              @else
                <li>
                    <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                 document.getElementById('logout-form').submit();">
                        Logout
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                </li>
              @endguest
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="nav-lg">
  <div class="nav-menu">
    <div class="large-menu-block">
      @include('shared.nav-links', ['pages', $pages])
    </div>
    <div class="nav-menu-logo">
      <img src="{{ Storage::url($_settings->logo)}}" alt="" >
    </div>
  </div>

  </div>
</div>
