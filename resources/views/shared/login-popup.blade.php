<!-- Button trigger modal -->
<button type="button" style="margin:0" class="btn btn-coupon btn-lg" data-toggle="modal" data-target="#myModal">
  JOIN US
</button>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">×</font>
          </font>
        </button>
        <span class="modal-title">
          <font style="vertical-align: inherit;">
            <font style="vertical-align: inherit;">Login</font>
          </font>
        </span>
      </div>
      <div class="modal-body">
          <div class="sclogin sourcecoast" id="sclogin-426">
            <div class="row-fluid">
          <div class="sclogin-joomla-login vertical span12">
            <form method="POST" action="{{ route('manual_auth') }}" id="sclogin-form426">
                <fieldset class="input-block-level userdata">
                    <div class="control-group" id="form-sclogin-username">
                      <div class="controls input-block-level">
                        <div class="input-append input-block-level">
                          {{ csrf_field() }}
                          <input name="email" tabindex="0" id="sclogin-username" class="input-block-level" alt="email" type="text" placeholder="Email" >
                        </div>
                      </div>
                    </div>
                      <div class="control-group" id="form-sclogin-password">
                        <div class="controls input-block-level">
                          <div class="input-append input-block-level">
                            <input name="password" tabindex="0" id="sclogin-passwd" class="input-block-level" alt="password" type="password" placeholder="Password">
                          </div>
                        </div>
                      </div>
                      <div class="control-group" id="form-sclogin-submitcreate">
                        <button type="submit" name="Submit" class="btn btn-primary ">
                          <font style="vertical-align: inherit;">
                            <font style="vertical-align: inherit;">Login</font></font>
                        </button>
                      <a class="btn" href="/register">
                        <font style="vertical-align: inherit;">
                          <font style="vertical-align: inherit;">Register</font>
                        </font>
                      </a>
                    </div>
                    <input id="sclogin-remember" type="hidden" name="remember" class="inputbox" checked="checked" value="yes" alt="Запомнить меня">
                    <input type="hidden" name="option" value="com_users"><input type="hidden" name="task" value="user.login"><input type="hidden" name="return" value="aHR0cHM6Ly9qZXJ1cy5jaXR5Lw=="><input type="hidden" name="mod_id" value="426"><input type="hidden" name="1108fe6028405c68711b1cb74878c860" value="1">            </fieldset>
                  </form>
                </div>
              </div>
            <div class="clearfix"></div>
          </div>
      </div>
    </div>
  </div>
</div>
