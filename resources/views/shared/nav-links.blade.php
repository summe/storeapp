@foreach ($pages as $page)
  <a href="{{ route('pages.show', ['id' => $page->slug])}}" class="menu-item-link">
    <div class="menu-item">
        {{ strtoupper($page->topic) }}
    </div>
  </a>
@endforeach
