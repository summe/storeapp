<div class="new-comment">
  <h2 class='comments-title'>ADD NEW COMMENT</h2>
  <form class="" action="{{ route('comments.store')}}" method="post">
    {{ csrf_field() }}
    <input type="hidden" name="coupon_id" value="{{ $coupon->id }}">
    <div class="form-group">
      <textarea name="text" rows="3" cols="30" class="form-control"></textarea>
    </div>
    <div class="form-group">
      <input type="submit" name="" class="btn btn-success" value="Add comment">
    </div>
  </form>
</div>
