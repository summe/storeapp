<div class="dropdown drop-menu-block">
  <button class="btn dropdown-toggle" type="button" data-toggle="dropdown">
    <i class="fa fa-bars" aria-hidden="true"></i>
    </button>
  <ul class="dropdown-menu dropdown-nav-item">
    <li><a href="/">Home</a></li>
    <li class="divider"></li>
    @foreach ($pages as $page)
      <li>
        <a href={{ route('pages.show', ['id' => $page->slug])}}>
          {{ $page->topic }}
        </a>
      </li>
      <li class="divider"></li>
    @endforeach
  </ul>
</div>
