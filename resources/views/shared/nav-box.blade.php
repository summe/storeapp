<div class="nav-slider small">
  <div class="nav-slider-title">
    <h2>CATEGORIES</h2>
  </div>
  <div class="nav-slider-text">
    <ul>
      @foreach ($pages as $page)
        <li>
          <a href="{{ route('pages.show', ['id'=> $page->slug])}}">
            <h4>
              <span>
                <i class=" {{'fa ' . $page->fa_fa_icon}}" aria-hidden="true"></i>
              </span>
              {{ $page->title }}
            </h4>
          </a>
        </li>
      @endforeach
    </ul>
  </div>
</div>
