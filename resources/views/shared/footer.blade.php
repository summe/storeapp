<div class="footer">
  <div class="footer-content">
    <div class="company-logo">
      <img src="http://clipart-library.com/image_gallery2/Globe-PNG.png" alt="" >
      <p class="company-name">{{ $_settings->company or 'company'}}</p>
    </div>
    <div class="footer-items">
      <div class="footer-item">
        <h2>{{ $_settings->footer_general_title or 'footer_general_title'}}</h2>
        <ul>
          @include('shared.main_nav')
        </ul>
      </div>
      <div class="footer-item">
        <h2>{{ $_settings->footer_categories_title or 'footer_categories_title'}}</h2>

        @foreach ($pages as $page)
          <li>
            <a href={{ route('pages.show', ['id' => $page->slug])}}>
              {{ ucfirst($page->topic) }}
            </a>
          </li>
        @endforeach
      </div>
    </div>
  </div>
  <div class="footer-copyrights">
    <h4>{{ $_settings->footer_rights or 'footer_rights'}}</h4>
  </div>
</div>
