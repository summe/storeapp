@extends('layouts.app')
@section('content')
  <div class="main-content">
    <div class="">
      <div class="inner-content">
        <div class="item-heading">
          {{ $info->title }}
        </div>
      </div>
    </div>
      <div class="inner-container">
      <div class="inner-content">
        <div class="item-container">
          <div class="item-content">
            {!!  $info->body !!}
          </div>
        </div>
      </div>
    </div>
    <div class="map-box">
      <div class="inner-content">
      </div>
    </div>

  </div>

  {{-- <div dir="rtl" data-slick='{"slidesToShow": 1, "slidesToScroll": 1}' id="carousel">
    <div><h3>1</h3></div>
    <div><h3>2</h3></div>
    <div><h3>3</h3></div>
    <div><h3>4</h3></div>
    <div><h3>5</h3></div>
    <div><h3>6</h3></div>
  </div>
  <script type="text/javascript">
    $(document).ready(function(){
      $('#carousel').slick({
         rtl: true
      });
    })
  </script> --}}
@endsection
