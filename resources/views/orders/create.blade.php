@extends('layouts.app')
@section('content')
<div class="request-form">
  <h1>Process order</h1>
  <form class="" action="{{ route('orders.store')}}" method="post">
    {{ csrf_field() }}
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
    @endif
    @guest
      <div class="form-group">
        <label for="">First name</label>
        <input required type="text" name="name" value="" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Last name</label>
        <input required type="text" name="last_name" value="" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Phone</label>
        <input required type="text" name="phone" value="" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Email </label>
        <input required type="text" name="email" value="" class="form-control">
      </div>
    @else
      <div class="form-group">
        <label for="">First name</label>
        <input required type="text" name="name" value="{{ auth()->user()->name }}" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Last name</label>
        <input required type="text" name="last_name" value="{{ auth()->user()->last_name }}" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Phone</label>
        <input required type="text" name="phone" value="{{ auth()->user()->phone }}" class="form-control">
      </div>
      <div class="form-group">
        <label for="">Email </label>
        <input required type="text" name="email" value="{{ auth()->user()->email }}" class="form-control">
      </div>
    @endguest
    <input type="submit" name="" value="Buy" class='btn btn-success'>
  </form>
</div>
@endsection
