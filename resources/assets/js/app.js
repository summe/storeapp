
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
// window.slick = require('slick-carousel/slick/slick.min')
// window.Vue = require('vue');
//
// /**
//  * Next, we will create a fresh Vue application instance and attach it to
//  * the page. Then, you may begin adding components to this application
//  * or customize the JavaScript scaffolding to fit your unique needs.
//  */
//
// Vue.component('example-component', require('./components/ExampleComponent.vue'));
//
// const app = new Vue({
//     el: '#app'
// });
document.addEventListener('DOMContentLoaded', function(){
  let toggle = document.getElementById('menu-toggle');
  let nav = document.getElementById('nav-slider');
  let closeBtn = document.getElementsByClassName('close-slider');
  if(toggle && nav && closeBtn){
    toggle.addEventListener('click', openNav)
    for (var i = 0; i < closeBtn.length; i++) {
      closeBtn[i].addEventListener('click', closeNav, false);
    }
    function openNav(){
      nav.classList.add('nav-open');
    }
    function closeNav(e){
      e = window.event || e;
      if(this === e.target) {
        if(nav.classList.contains('nav-open')){
          nav.classList.remove('nav-open');
        }
      }
      event.stopPropagation();
    }
  }
});
